/**
 * Created by Invia on 13.02.2017.
 */
function myFunction() {
    var x = document.getElementById("myText").value;
    document.getElementById("demo").innerHTML = Math.floor(Math.random() * x);
}

function clearmyFunction() {
    $("#myText").val('');
    $("#demo").text('');                   //сброс значений в текстовом поле на странице randomize;
}


function myFunctionMaxVal() {
    var arr = document.getElementById("myTextMaxValue").value;
    var y = arr.split(',');
    var mathMax=Math.max.apply(null, y); //преобразование в массив чисел
    console.log(mathMax);
    if (isNaN(mathMax)) {
        document.getElementById("alertErr").innerHTML =alert('Warning! Wrong input format');
    }
    else {
        document.getElementById("demoMaxValue").innerHTML = Math.max.apply(null, y);
    }
}

function clearmyFunctionMaxVal() {
    $("#myTextMaxValue").val('');
    $("#demoMaxValue").text('');         //сброс значений в текстовом поле на странице maxVal;
}


/*if (x < 0) {
//   document.getElementById("demo").innerHTML = Math.floor(Math.random() * x);
//}if (100 < x < 1000) {
//    document.getElementById("demo").innerHTML = Math.floor(Math.random() * x);
//}
//if (1000 < x < 10000) {
//    document.getElementById("demo").innerHTML = Math.floor(Math.random() * x);
//}
//if (10000 < x < 100000) {
//    document.getElementById("demo").innerHTML = Math.floor(Math.random() * x);
//}
//if (100000 < x < 1000000) {
//    document.getElementById("demo").innerHTML = Math.floor(Math.random() * x);
//}
//if (100000 < x) {
//    document.getElementById("demo").innerHTML = Math.floor(Math.random() * x);
////}
//} */
